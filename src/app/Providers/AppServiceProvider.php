<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\FormatterService;
use App\Services\InputClass;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Services\FormatterService', function() {
            return new FormatterService();
        });
        $this->app->bind('Services\InputClass', function() {
            return new InputClass();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
