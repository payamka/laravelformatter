<?php

namespace App\Contracts;

use App\Services\FormatterService;
use App\Services\InputClass;

interface FormatterServiceInterface
{
    function input($input);
    function columns(array $columns): FormatterService;
    function searchable($searchables): FormatterService;
    function sortable($sortables): FormatterService;
    function paginate(array $paginate): FormatterService;
    function get(): string;
}
