<?php

namespace App\Services;

use App\Contracts\FormatterServiceInterface;

class FormatterService implements FormatterServiceInterface
{
    public function input($input): FormatterService
    {
        $this->object = app()->make('Services\InputClass')->input($input);
        return $this;
    }

    public function columns(array $columns): FormatterService
    {
        $this->object->columns($columns);
        return $this;
    }

    public function searchable($searchables): FormatterService
    {
        $this->object->searchable($searchables);
        return $this;
    }

    public function sortable($sortables): FormatterService
    {
        $this->object->sortable($sortables);
        return $this;
    }

    public function paginate(array $paginate): FormatterService
    {
        $this->object->paging($paginate);
        return $this;
    }

    public function get(): string
    {
        return (string)$this->object;
    }
}
