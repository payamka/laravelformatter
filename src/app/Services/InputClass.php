<?php

namespace App\Services;

class InputClass
{
    private $columns = [];
    private $searchables = [];
    private $sortables = [];
    private $paginate = [];

    public function input($values): InputClass
    {
        $this->values = json_decode($values);
        return $this;
    }

    public function columns(array $columns)
    {
        $this->columns = $columns;
    }

    public function searchable($searchables)
    {
        $this->searchables = $searchables;
    }

    public function sortable($sortables)
    {
        $this->sortables = $sortables;
    }

    public function paging(array $paginate)
    {
        $this->paginate = $paginate;
    }

    public function __toString(): string
    {
        $this->prepare();
        $this->getColumn();
        unset($this->values);
        return json_encode($this);
    }

    public function prepare()
    {
        $this->search();
        $this->sort();
        $this->paginate();
        $this->metadata();
    }

    private function getColumn()
    {
        foreach ($this->columns as $columnKey => $columnValue) {
            $this->$columnKey($columnValue);
        }
    }

    public function __call(string $name, array $arguments): InputClass
    {
        $this->setProperty($arguments[0], $this->getValue($name));
        return $this;
    }

    private function getValue($path): mixed
    {
        $propertyValue = $this->values;
        foreach (explode('.', $path) as $path) {
            if (isset($propertyValue->$path)) {
                $propertyValue = $propertyValue->$path;
            } else {
                if (isset($prevValue[$path])) {
                    $propertyValue = $prevValue[$path];
                }
            }

            $prevValue = $propertyValue;
        }

        return $propertyValue;
    }

    private function setProperty(string $path, mixed $value)
    {
        $desiredPath = explode('.', $path);
        $count = count($desiredPath);
        $index = count($desiredPath) - 1;

        while ($index >= 0) {
            $current = $desiredPath[$index];
            $tempObject = new \stdClass();
            if ($count == 1) {
                $this->$current = $value;
            } else {
                if ($index == ($count - 1)) {
                    $tempObject->$current = $value;
                } else if ($index == 0) {
                    $this->$current = $prevObject;
                } else {
                    $tempObject->$current = $prevObject;
                }
            }

            $prevObject = $tempObject;
            $index--;
        }
    }

    public function search()
    {
        foreach ($this->searchables as $searchableKey => $searchableVariable) {
            if (isset($_GET[$searchableVariable])) {
                $value = $this->values;
                foreach (explode('.', $searchableKey) as $path) {
                    if (is_array($value)) {
                        $filtered = collect($value);
                        $filtered = $filtered->filter(function ($value, $key) use ($path, $searchableVariable) {
                            return $value->$path == $_GET[$searchableVariable];
                        });
                        $filtered = $filtered->values()->all();
                    } else {
                        $value = $value->$path;
                    }

                }

                $this->addToList($searchableKey, $filtered);
            }
        }
    }

    public function sort()
    {
        $sortables = $this->sortables;
        if (isset($_GET['sortBy']) && isset($sortables[$_GET['sortBy']])) {
            $sortableKey = $sortables[$_GET['sortBy']];

            $value = $this->values;
            foreach (explode('.', $sortableKey) as $path) {
                if (is_array($value)) {
                    $sorted = collect($value);
                    if ($_GET['sortType'] == 'desc') $sorted = $sorted->sortByDesc($path); else
                        $sorted = $sorted->sortBy($path);

                    $sorted = $sorted->values()->all();
                } else {
                    $value = $value->$path;
                }
            }

            $this->addToList($sortableKey, $sorted);
        }
    }

    public function paginate()
    {
        $paginate = $this->paginate;
        $path = $paginate[0];
        $count = $paginate[1];
        $page = $_GET['page'] ?? 1;

        $propertyValue = $this->values;
        foreach (explode('.', $path) as $path) {
            if (isset($propertyValue->$path)) {
                $propertyValue = $propertyValue->$path;
            }
        }

        $propertyValue = collect($propertyValue)->forPage($page, $count)->values()->all();

        $this->addToList($path, $propertyValue);
    }

    private function addToList($fullPath, $newList)
    {
        $fullPathArray = explode('.', $fullPath);
        $firstKey = $fullPathArray[0];
        array_shift($fullPathArray);
        $pointer = &$this->values->$firstKey;
        $index = 0;
        do {
            if (is_array($pointer)) {
                $pointer = $newList;
            } else {
                $path = $fullPathArray[$index];
                $pointer = &$pointer->$path;
            }
            $index++;
        } while (count($fullPathArray) > $index);
    }

    public function metadata()
    {
        $this->metadata = [
            'searchables' => array_values($this->searchables),
            'sortables' => array_keys($this->sortables),
            'page_size' => ($this->paginate)[1]
        ];
    }
}
