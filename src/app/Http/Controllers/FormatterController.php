<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormatterController extends Controller
{
    public function call(Request $request)
    {
        $input = file_get_contents("../input.json");

        $formatter = app()->make('Services\FormatterService');
        $formatter->input($input)->searchable([
            'employees.position' => 'position',
            'employees.salary' => 'salary'
            ])
            ->sortable([
                'experience' => 'employees.experience',
                'salary' => 'employees.salary'
            ])
            ->paginate([
                'employees', 2
            ])
            ->columns([
                'date' => 'ListDate',
                'employees' => 'people.all'
            ]);

        return $formatter->get();
//        return response()->json($formatter->get());
    }
}
